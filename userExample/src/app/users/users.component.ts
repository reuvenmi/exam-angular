import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users;
  usersKeys = [];

  optimisticAdd(users){
    var newKey = this.usersKeys[this.usersKeys.length-1]+1;
    console.log(this.usersKeys.length);
    console.log(this.usersKeys);
    var newUserObject = {};
    newUserObject['body'] = users;
    console.log(newUserObject);
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);
    console.log(this.users);
    console.log(event,newKey);
  }

  pessimiaticAdd(){
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });   
  }


  constructor(private service:UsersService) {
    this.service.getUsers().subscribe(response=>{
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });
   }

  ngOnInit() {
  }

}
