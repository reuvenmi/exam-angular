import { UsersService } from '../../users.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Output()
   addUser: EventEmitter<any> = new EventEmitter<any>();
  
    @Output()
  
   addUserPs: EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;
  
  usrform = new FormGroup({
    name: new FormControl('', Validators.required),
    phonenumber: new FormControl('', Validators.required)
  });

  sendData(){
    this.addUser.emit(this.usrform.value.name);
    this.service.postUser(this.usrform.value).subscribe(response =>{
  //console.log(response);
   this.addUserPs.emit();
    console.log(response);
    });
  }

  constructor(service:UsersService) { 
    this.service = service;
  }

  ngOnInit() {
  }

  

}