import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { HttpModule } from '@angular/http';
import { UsersService } from './users.service';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UserFormComponent } from './users/user-form/user-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FireUsersComponent } from './fire-users/fire-users.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    UsersComponent,
    NotFoundComponent,
    NavigationComponent,
    UserFormComponent,
    FireUsersComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'user', component:UsersComponent}, //default - localhost:4200 - homepage
      {path:'fireusers', component:FireUsersComponent},
      {path:'', component:ProductsComponent}, //localhost:4200/products
      {pathMatch:'full', path:'users-firebase', component:FireUsersComponent},
      {path:'**', component:NotFoundComponent} //all the routs that donwt exist
    ])
  ],
  providers: [
     UsersService ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
