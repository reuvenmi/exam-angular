import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class UsersService {
  
  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/slimTEST/users');
  }
  getUsersFire(){
    return this.db.list('/users').valueChanges();    
     }
  postUser(data){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
    return this.http.post('http://localhost/slimTEST/users',params.toString(),options); 
  }
  constructor(private http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }
  
}