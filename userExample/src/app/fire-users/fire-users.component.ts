
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'fire-users',
  templateUrl: './fire-users.component.html',
  styleUrls: ['./fire-users.component.css']
})
export class FireUsersComponent implements OnInit {

  users;

  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(users =>{
      this.users = users;
      console.log(this.users);
    });
  }

}